from django.urls import resolve
from django.test import TestCase

# Create your tests here.
from .views import *


class HomePageTest(TestCase):
    def test_uses_heroes_template(self):
        response = self.client.get('/')
        self.assertTemplateUser(response, 'heroes.html')
