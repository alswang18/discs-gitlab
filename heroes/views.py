
from django.shortcuts import render, redirect
from django.http import HttpResponse


def home_page(request):
    return redirect('heroes')


def heroes(request):
    return render(request, 'heroes.html')


def hero_cloud(request):
    return render(request, 'detail_cloud.html')


def hero_sunflowey(request):
    return render(request, 'detail_sunflowey.html')


def hero_jester(request):
    return render(request, 'detail_jester.html')
