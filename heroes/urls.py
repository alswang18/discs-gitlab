
from django.contrib import admin
from django.conf.urls import include, url
from .views import *

urlpatterns = [
    url(r'^$', home_page, name="home_page"),
    url(r'^heroes/$', heroes, name="heroes"),
    url(r'^hero/cloud$', hero_cloud, name="hero_cloud"),
    url(r'^hero/sunflowey$', hero_sunflowey, name="hero_sunflowey"),
    url(r'^hero/jester$', hero_jester, name="hero_jester"),
]
